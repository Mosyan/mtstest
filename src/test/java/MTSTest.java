import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;


public class MTSTest {
    MSKHomePage page = new MSKHomePage(new ChromeDriver());

    @BeforeTest
    public void setup() {
        page.addImplicitlyWait(10);
        page.addPageLoadyWait(20);
        page.setWindowSize(1280, 1024);

    }

    @Test(groups = {"ScenarioOne"})
    public void getURLTest() {
        page.getURL();
    }

    @Test(dependsOnMethods = {"getURLTest"}, groups = {"ScenarioOne"})
    public void getChangeRegionTest() {
        page.getChangeRegion();
    }

    @Test(dependsOnMethods = {"getChangeRegionTest"}, groups = {"ScenarioOne"})
    public void closeChangeRegionTest() {
        page.closeChangeRegion();
    }

    @Test(dependsOnGroups = {"ScenarioTwo"})
    public void changeRegionCountryListTest() {
        page.getURL();
        page.getChangeRegion();
        List<String> countries = page.getChangeRegionCountryList();
        Assert.assertTrue(countries.contains("Россия"));
        Assert.assertTrue(countries.contains("Армения"));
        Assert.assertFalse(countries.contains("США"));
    }

    @Test(dependsOnGroups = {"ScenarioTwo"})
    public void changeRegionRegionListTest() {
        page.getURL();
        page.getChangeRegion();
        Assert.assertEquals(page.getChangeRegionRegionList().size(), 80);
    }

    @Test(dependsOnGroups = {"ScenarioTwo"})
    public void changeRegionDialogListTest() {
        page.getURL();
        page.getChangeRegion();
        page.setChangeRegionDialog("Mосква");
        page.getChangeRegionDialogList().forEach(text -> Assert.assertTrue(text.contains("Москва")));
    }

    @Test(groups = {"ScenarioTwo"})
    public void changeRegionCityListTest() {
        page.getURL();
        page.getChangeRegion();
        page.pressNOblLink();
        List<String> countries = page.getChangeRegionCityList();
        Assert.assertTrue(countries.contains("Новосибирск"));
    }

    @Test(dependsOnMethods = {"changeRegionCityListTest"}, groups = {"ScenarioTwo"})
    public void urlNSKTest() {
        page.pressNovosibirskLink();
        Assert.assertEquals(page.getCurrentUrl(), "https://nsk.mts.ru/personal");
    }

    @Test(dependsOnGroups = {"ScenarioOne"})
    public void moveToTarifLinkTest() {
        page.getURL();
        page.moveToTarifLink();
    }

    @Test(dependsOnGroups = {"ScenarioOne"})
    public void moveFromTarifLinkTest() {
        page.getURL();
        for (int i = 0; i < 10; i++) {
            page.moveToTarifLink();
            page.moveToMTSGOLink();
        }
    }

}
