import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MSKHomePage {
    private static String url = "https://moskva.mts.ru/personal";
    WebDriver driver;
    private By changeRegionUnvisible = By.cssSelector("div[class='popup-modal js-mts16-popup-regions']");
    private By changeRegionPopAp = By.cssSelector("div[class='popup-modal js-mts16-popup-regions is-open']");
    private By changeRegionButton = By.cssSelector("button[class='btn btn_gray region-confirm__btns-item']");
    private By exitRegionChoice = By.cssSelector("div[class='mts16-popup__close']");
    private By country = By.cssSelector("div[class='mts16-popup-regions__col-categories'] a");
    private By region = By.cssSelector("div[class='mts16-popup-regions__group'] a");
    private By findRegionOrCityDialog = By.cssSelector("input[class='mts16-input ui-autocomplete-input']");
    private By findRegionOrCityDialogPopApElem = By.cssSelector("li[class='mts16-popup-regions__dropdown-list ui-menu-item'] span[class='mts16-popup-regions__dropdown-content']");
    private By NovosibirskayaObl = By.linkText("Новосибирская область");
    private By city = By.cssSelector("div[class='mts16-popup-regions__col-subregions']  a");
    private By Novosibirsk = By.linkText("Новосибирск");
    private By tarifLink = By.cssSelector("img[title='Тарифы и услуги']");
    private By tarifStocks = By.cssSelector("a[data-context='Тарифы и услуги // Домашний интернет и ТВ // Акции']");
    private By tarifPopAp = By.cssSelector("li[data-alias='tarifi-i-uslugi'] >div");
    private By goToMTSLink = By.cssSelector("img[title='Перейти в МТС']");

    MSKHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public MSKHomePage getURL() {
        try {
            driver.get(url);
        } catch (org.openqa.selenium.TimeoutException e) {
            Assert.fail("web page did not load");
        }
        return this;
    }

    public MSKHomePage pressChangeRegionButton() {
        try {
            driver.findElement(changeRegionButton).click();
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("button did not find");
        }
        return this;
    }

    public MSKHomePage getChangeRegion() {
        pressChangeRegionButton();
        try {
            driver.findElement(changeRegionPopAp);
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find");
        }
        return this;
    }

    public MSKHomePage closeChangeRegion() {
        try {
            driver.findElement(exitRegionChoice).click();
            driver.findElement(changeRegionButton);
            driver.findElement(changeRegionUnvisible);
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find");
        }
        return this;
    }

    public List<String> getChangeRegionCountryList() {
        try {
            List<WebElement> elements = driver.findElements(country);
            List<String> elementsText = new ArrayList<String>();
            for (WebElement element : elements) {
                elementsText.add(element.getText());
            }
            return elementsText;
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find in");
            return new ArrayList<String>();
        }
    }

    public List<WebElement> getChangeRegionRegionList() {
        try {
            List<WebElement> elements = driver.findElements(region);
            return elements;
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find");
            return new ArrayList<WebElement>();
        }
    }

    public MSKHomePage setChangeRegionDialog(String message) {
        driver.findElement(findRegionOrCityDialog).sendKeys(message);
        return this;
    }

    public List<String> getChangeRegionDialogList() {
        try {
            List<WebElement> elements = driver.findElements(findRegionOrCityDialogPopApElem);
            List<String> elementsText = new ArrayList<String>();
            for (WebElement element : elements) {
                elementsText.add(element.getAttribute("innerText"));
            }
            return elementsText;
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find");
            return new ArrayList<String>();
        }
    }

    public MSKHomePage pressNOblLink() {
        try {
            Wait<WebDriver> wait = (new WebDriverWait(driver, 30));
            wait.until(ExpectedConditions.presenceOfElementLocated(NovosibirskayaObl));
            driver.findElement(NovosibirskayaObl).click();
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("link did not find");
        }
        return this;
    }

    public List<String> getChangeRegionCityList() {
        try {
            List<WebElement> elements = driver.findElements(city);
            List<String> elementsText = new ArrayList<String>();
            for (WebElement element : elements) {
                elementsText.add(element.getAttribute("innerText"));
            }
            return elementsText;
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("element did not find");
            return new ArrayList<String>();
        }
    }

    public MSKHomePage pressNovosibirskLink() {
        try {
            driver.findElement(Novosibirsk).click();
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("button did not find");
        }
        return this;
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public MSKHomePage moveToTarifLink() {
        try {
            new Actions(driver).moveToElement(driver.findElement(tarifLink)).build().perform();
            driver.findElement(tarifStocks);
            driver.findElement(tarifPopAp);
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("button did not find");
        }
        return this;
    }

    public MSKHomePage moveToMTSGOLink() {
        try {
            new Actions(driver).moveToElement(driver.findElement(goToMTSLink)).build().perform();
        } catch (org.openqa.selenium.TimeoutException | NoSuchElementException e) {
            Assert.fail("button did not find");
        }
        return this;
    }

    public MSKHomePage addImplicitlyWait(Integer secondToWait) {
        driver.manage().timeouts().implicitlyWait(secondToWait, TimeUnit.SECONDS);
        return this;
    }
    public MSKHomePage addPageLoadyWait(Integer secondToWait) {
        driver.manage().timeouts().pageLoadTimeout(secondToWait, TimeUnit.SECONDS);
        return this;
    }

    public MSKHomePage setWindowSize(Integer width, Integer heigh) {
        driver.manage().window().setSize(new Dimension(width, heigh));
        return this;
    }
}